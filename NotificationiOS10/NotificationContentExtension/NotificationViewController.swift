//
//  NotificationViewController.swift
//  NotificationContentExtension
//
//  Created by Crasneanu Cristian on 21/11/16.
//  Copyright © 2016 Crasneanu Cristian. All rights reserved.
//

import UIKit
import UserNotifications
import UserNotificationsUI

class NotificationViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    
    func setupWebView(url: URL) {
        do {
            let attachment = try Data(contentsOf: url) 
            webView.load(attachment, mimeType: "image/gif", textEncodingName: String(), baseURL: url)
            webView.isUserInteractionEnabled = false
            
        } catch {
            print(error.localizedDescription)
        }
    }
    
}

extension NotificationViewController: UNNotificationContentExtension {
    public func didReceive(_ notification: UNNotification) {
        titleLabel.text = notification.request.content.title
        subtitleLabel.text = notification.request.content.subtitle
        bodyLabel.text = notification.request.content.body
        let attachment = notification.request.content.attachments[0]
        setupWebView(url: attachment.url)
    }
}
