//
//  ViewController.swift
//  NotificationiOS10
//
//  Created by Crasneanu Cristian on 15/11/16.
//  Copyright © 2016 Crasneanu Cristian. All rights reserved.
//

import UIKit
import UserNotifications

struct RequestIdentifier {
    static let richRequestIdentifier = "richRequestIdentifier"
    static let mediaAttachmentRequestIdentifier = "mediaIdentifier"
    static let customContentIdentifier = "customContentIdentifier"
    static let delayedIdentifier = "delayedIdentifier"
    static let customAction = "customAction"
    static let dismissAction = "dismissAction"

}


struct ActionIdentifier {
    static let remindMeLater = "remindMeLater"
}

struct CategoryIdentifier {
    static let customUserInterface = "customUserInterface"
    static let remindMeLater = "reminder"
    static let customAction = "customAction"
}

class ViewController: UIViewController {

   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationiOS10.setupDelegate(viewControllerDelegate: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func requestAuthorizationAction(_ sender: UIButton) {
        NotificationiOS10.requestAuthorization()
    }
    
    @IBAction func getNotificationCenterAction(_ sender: UIButton) {
        NotificationiOS10.getNotificationSettings()
    }

    @IBAction func showRichNotificationAction(_ sender: UIButton) {
        NotificationiOS10.showRichNotification()
    }
    
    @IBAction func removePendingNotificationAction(_ sender: UIButton) {
        NotificationiOS10.removePendingNotification()
    }
    
    @IBAction func updatePendingNotificationAction(_ sender: UIButton) {
        NotificationiOS10.updatePendingNotification()
    }
    
    @IBAction func removeDeliveredNotificationAction(_ sender: UIButton) {
        NotificationiOS10.removeDeliveredNotification(arrayRequestIdentifiers: [RequestIdentifier.richRequestIdentifier])
    }
    
    @IBAction func updateDeliveredNotificationAction(_ sender: UIButton) {
        NotificationiOS10.updateDeliveredRichNotification()
    }

    @IBAction func deleteAllNotifications(_ sender: UIButton) {
        NotificationiOS10.deleteAllNotifications()
    }
    
    @IBAction func showMediaAttachementNotification(_ sender: UIButton) {
        NotificationiOS10.showMediaAttachmentNotification(timeInterval: 5)
    }
    
    @IBAction func showCustomNotification(_ sender: UIButton) {
        NotificationiOS10.showCustomContentNotification()
    }
    
    // MARK: Notification actions
    
    @IBAction func showNotificationWithCustomAction(_ sender: UIButton) {
        NotificationiOS10.showNotificationWithCustomAction()
    }
    
}


extension ViewController: UNUserNotificationCenterDelegate {
    
    public func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Swift.Void) {
        completionHandler( [.alert, .badge, .sound])
       
    }
    
    public func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Swift.Void) {
        print("Tapped in notification")
        if response.actionIdentifier == ActionIdentifier.remindMeLater {
            NotificationiOS10.showMediaAttachmentNotification(timeInterval: 10)
        }
    }
}

