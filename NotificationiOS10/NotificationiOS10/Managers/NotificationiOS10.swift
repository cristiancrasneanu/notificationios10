//
//  NotificationCenter.swift
//  NotificationiOS10
//
//  Created by Crasneanu Cristian on 15/11/16.
//  Copyright © 2016 Crasneanu Cristian. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import UserNotifications

class NotificationiOS10 {
    
    class func setupDelegate(viewControllerDelegate: UNUserNotificationCenterDelegate) {
        UNUserNotificationCenter.current().delegate = viewControllerDelegate
    }
    
    class func requestAuthorization() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { granted, error in }
    }
    
    class func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            print(settings)
        }
    }
    
    class func registerForRemoteNotifications() {
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    
    class func showRichNotification() {
        let content = UNMutableNotificationContent()
        content.title = "Title for notification"
        content.subtitle = "Subtitle for notification"
        content.body = "Body for notification"
        content.badge = 1
        
        let timeIntervalTrigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        
        //let calendarTrigger = UNCalendarNotificationTrigger(dateMatching: DateComponents(), repeats: false)
        
        //let locationTrigger = UNLocationNotificationTrigger(region: CLRegion(), repeats: false)
        
        let request = UNNotificationRequest(identifier: RequestIdentifier.richRequestIdentifier , content: content, trigger: timeIntervalTrigger)
        UNUserNotificationCenter.current().add(request) { error in
            if (error != nil) {
                print("Error \(error?.localizedDescription)")
            }
        }
    }
    
    class func removePendingNotification() {
        UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [RequestIdentifier.richRequestIdentifier])
    }
    
    class func updatePendingNotification() {
        let content = UNMutableNotificationContent()
        content.title = "Title for Update Pending Notification"
        content.subtitle = "Subtitle for Update Pending Notification"
        content.body = "Body for Update Pending Notification"
        content.badge = 1
        
        let timeIntervalTrigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        
        let request = UNNotificationRequest(identifier: RequestIdentifier.richRequestIdentifier , content: content, trigger: timeIntervalTrigger)
        UNUserNotificationCenter.current().add(request) { error in
            if (error != nil) {
                print("Error \(error?.localizedDescription)")
            }
        }
    }
    
    class func updateDeliveredRichNotification() {
        let content = UNMutableNotificationContent()
        content.title = "Title for Update Delivered Notification"
        content.subtitle = "Subtitle for Update Delivered Notification"
        content.body = "Body for Update Delivered Notification"
        content.badge = 1
        
        let timeIntervalTrigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        
        let request = UNNotificationRequest(identifier: RequestIdentifier.richRequestIdentifier , content: content, trigger: timeIntervalTrigger)
        UNUserNotificationCenter.current().add(request) { error in
            if (error != nil) {
                print("Error \(error?.localizedDescription)")
            }
        }
    }
    
    class func removeDeliveredNotification(arrayRequestIdentifiers: [String]) {
        UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: arrayRequestIdentifiers)
    }
    
    class func deleteAllNotifications() {
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
    }
    
    class func showMediaAttachmentNotification(timeInterval: TimeInterval) {

        let content = UNMutableNotificationContent()
        content.title = "Improve Vision!"
        content.subtitle = "8 Directions!" 
        content.body = "Just a reminder to do your exercises!"

        if let path = Bundle.main.path(forResource: "giphy", ofType: "gif") {
            let url = URL(fileURLWithPath: path)
            
            do {
                let attachment = try UNNotificationAttachment(identifier: "image", url: url, options: nil)
                content.attachments = [attachment]
            } catch {
                print("The attachment was not loaded.")
            }
        }
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: timeInterval, repeats: false)
        let request = UNNotificationRequest(identifier: RequestIdentifier.mediaAttachmentRequestIdentifier, content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request) {(error) in
            if let error = error {
                 print("Error \(error.localizedDescription)")
            }
        }
    }
    
    class func showCustomContentNotification() {
        let customNotification = UNMutableNotificationContent()
        customNotification.title = "Custom Notification"
        customNotification.subtitle = "Subtitle for Custom Notification"
        customNotification.body = "Body for Custom Notification"
        customNotification.categoryIdentifier = CategoryIdentifier.customUserInterface
        customNotification.threadIdentifier = "threadIdentifier"
        
        if let path = Bundle.main.path(forResource: "giphy", ofType: "gif") {
            let url = URL(fileURLWithPath: path)
            
            do {
                let attachment = try UNNotificationAttachment(identifier: "image", url: url, options: nil)
                customNotification.attachments = [attachment]
            } catch {
                print("The attachment was not loaded.")
            }
        }
        
        let request = UNNotificationRequest(identifier: RequestIdentifier.customContentIdentifier, content: customNotification, trigger: nil)
        UNUserNotificationCenter.current().add(request) { error in
            if let error = error {
                print("Error \(error.localizedDescription)")
            }
        }
    }
    
    class func showNotificationWithCustomAction() {
        let action = UNNotificationAction(identifier: ActionIdentifier.remindMeLater , title: "Remind me later")
        
        let category = UNNotificationCategory(identifier: CategoryIdentifier.remindMeLater, actions: [action], intentIdentifiers: [])
        UNUserNotificationCenter.current().setNotificationCategories([category])
        
        
        let content = UNMutableNotificationContent()
        content.title = "Title for Notification with Custom action"
        content.subtitle = "Subtitle for Notification with Custom action"
        content.body = "Body for Update Notification with Custom action"
        content.badge = 1
        content.categoryIdentifier = CategoryIdentifier.remindMeLater
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        
        let request = UNNotificationRequest(identifier: RequestIdentifier.customAction , content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request) { error in
            if (error != nil) {
                print("Error \(error?.localizedDescription)")
            }
        }

    }
    
    
}
