//
//  NotificationService.swift
//  NotificationService
//
//  Created by Crasneanu Cristian on 11/12/16.
//  Copyright © 2016 Crasneanu Cristian. All rights reserved.
//

import UserNotifications

class NotificationService: UNNotificationServiceExtension {

    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?

    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        
        if let bestAttemptContent = bestAttemptContent {
            // Modify the notification content here...
            bestAttemptContent.title = "\(bestAttemptContent.title) [modified]"
            
            contentHandler(bestAttemptContent)
        }
        
        
        let fileURL = URL(string: "www.google.com/image.png")
        do {
            let attachment = try UNNotificationAttachment(identifier: "image", url: fileURL!, options: nil)
            let content = request.content.mutableCopy as! UNMutableNotificationContent
            content.attachments = [attachment]
            contentHandler(content)
        } catch {
            
        }
        
        
    }
    
    override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }

}
